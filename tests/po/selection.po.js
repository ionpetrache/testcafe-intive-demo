import { Selector } from 'testcafe';

export default class SelectionPage {
    constructor() {
        this.url = 'https://www.thetestroom.com/jswebapp/animalselection.html';
        this.title = Selector('title');
        this.backButton = Selector('#back_button');
        this.continueButton = Selector('#continue_button');
        this.animalSelect = Selector('select[ng-model]');
        this.animalSelectOption = this.animalSelect.find('option');
    }
}
