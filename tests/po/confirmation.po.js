import { Selector } from 'testcafe';

export default class ConfirmationPage {
    constructor() {
        this.url = 'https://www.thetestroom.com/jswebapp/confirm.html';
        this.title = Selector('title');
        this.headerOneText = Selector('h1');
        this.paragraphText = Selector('hr~p');
        this.backToHomeButton = Selector('#back_button');
    }
}
