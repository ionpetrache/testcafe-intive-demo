import { Selector } from 'testcafe';

export default class HomePage {
    constructor() {
        this.url = 'https://www.thetestroom.com/jswebapp/index.html';
        this.title = Selector('title');
        this.nameInput = Selector('input[ng-model="person.name"]');
        this.continueButton = Selector('#continue_button');
        this.nameHeader = Selector('h2[ng-bind="person.name"]');
    }
}
