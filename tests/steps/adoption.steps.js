import HomePage from '../po/home.po';
import SelectionPage from '../po/selection.po';
import ConfirmationPage from '../po/confirmation.po';

const homePage = new HomePage();
const selectionPage = new SelectionPage();
const confirmationPage = new ConfirmationPage();

async function enterNameStep(t, name) {
    // Given
    await t
        .expect(homePage.title.innerText).eql('Zoo Adoption | Home')
    // When
        .typeText(homePage.nameInput, name);
}

async function verifyNameIsDisplayed(t, name) {
    // Then
    await t.expect(homePage.nameHeader.innerText).eql(name)
        .expect(homePage.title.innerText).eql('Zoo Adoption | Home');
}

async function continueFromHome(t) {
    // When
    await t.click(homePage.continueButton)
    // Then
        .expect(selectionPage.title.innerText).eql('Zoo Adoption | Select your Animal');
}

async function selectAnimalStep(t, animalName) {
    // When
    await  t.click(selectionPage.animalSelect)
        .click(selectionPage.animalSelectOption.withText(animalName))
        .click(selectionPage.continueButton)
    // Then
        .expect(confirmationPage.title.innerText).eql('Zoo Adoption | Confirmation')
        .expect(confirmationPage.paragraphText.innerText)
        .eql('Thank you for your selection. Your animal adoption papers will be sent to you shortly with a lovely card from your selected animal.');
}

async function returnToHome(t) {
    // When
    await t.click(confirmationPage.backToHomeButton)
    // Then
        .expect(homePage.title.innerText).eql('Zoo Adoption | Home');
}


export {enterNameStep, verifyNameIsDisplayed, continueFromHome, selectAnimalStep, returnToHome};