import HomePage from './po/home.po';
import {enterNameStep, verifyNameIsDisplayed, continueFromHome, selectAnimalStep, returnToHome} from './steps/adoption.steps';

const homePage = new HomePage();

fixture `Zoo Adoption story`
    .page(homePage.url);

test('Scenario 1: A user should be able to adopt Nemo the Fish', async t => {
    await enterNameStep(t, 'Ion');
    await verifyNameIsDisplayed(t, 'Ion');
    await continueFromHome(t);
    await selectAnimalStep(t,'Nemo the Fish');
});

test('Scenario 2: A user should be able to adopt another animal after other complete adoption', async t => {
    await enterNameStep(t, 'Ion');
    await verifyNameIsDisplayed(t, 'Ion');
    await continueFromHome(t);
    await selectAnimalStep(t,'George the Turtle');
    await returnToHome(t);
    await enterNameStep(t, 'Ion');
    await verifyNameIsDisplayed(t, 'Ion');
    await continueFromHome(t);
    await selectAnimalStep(t,'Simba the Lion');
    await returnToHome(t);
});

test.skip('Scenario 3: A user should not be able to adopt when he does not provide the name', async t => {
    await t
        .expect(homePage.title.innerText).eql('Zoo Adoption | Home');
    await continueFromHome(t);
    await t.expect(homePage.title.innerText).eql('Zoo Adoption | Home');
});

